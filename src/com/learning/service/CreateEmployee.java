package com.learning.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.learning.Employee;

public class CreateEmployee {
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Eclipselink_JPA");
		
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		Employee emp =  new Employee(); //
		emp.setEid(1111);
		emp.setEname("Devendra");
		emp.setSalary(1000000.00);
		emp.setDeg("Software Engineer");
				
		//let's create multiplse user's
		
		Employee emp1 =  new Employee(); //
		emp1.setEid(1112);
		emp1.setEname("Thapa");
		emp1.setSalary(1000000.00);
		emp1.setDeg("Computer Science -  Engineer");
		
		Employee emp2 =  new Employee(); //
		emp2.setEid(1113);
		emp2.setEname("Magar");
		emp2.setSalary(1000000.00);
		emp2.setDeg("Network Engineer");
		
		Employee emp3 =  new Employee(); //
		emp3.setEid(1114);
		emp3.setEname("Thapa Magar");
		emp3.setSalary(1000000.00);
		emp3.setDeg("Software Engineer");
		
		Employee emp4 =  new Employee(); //
		emp4.setEid(1115);
		emp4.setEname("Dvndra");
		emp4.setSalary(1000000.00);
		emp4.setDeg("Desinger");
		
		Employee emp5 =  new Employee(); //
		emp5.setEid(1116);
		emp5.setEname("Niru");
		emp5.setSalary(1000000.00);
		emp5.setDeg("IT desk support");
		
		em.persist(emp);
		em.persist(emp1);
		em.persist(emp2);
		em.persist(emp3);
		em.persist(emp4);
		em.persist(emp5);
		
		em.getTransaction().commit();
		
		em.close();
		emf.close();
	}

}
