package com.learning.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.learning.Employee;

/**
 * will apply scalar and aggregate function of JPQL
 * Scalar function return resultant values based on input values
 * Aggregate function return the resultant values by calculating the inputes values
 * 
 * @author evers
 *
 */
public class ScalarAggregateFunction {

	static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Eclipselink_JPA");
	
	static EntityManager em = emf.createEntityManager();
	
	public static void main(String[] args) {
	
//		showBetweenFunction();
		showOrdering();
		
		
	}
	
	public void showScalarFunction() {
		//Scalar function
				Query query  = em.createQuery("Select UPPER(e.ename) from Employee e");
				
				List<String> list = query.getResultList();
				
				System.out.println("Scalar function Results: \n");
				for(String e:list) {
					System.out.println("Employee name: " +e);
					
				}
	}
	public void showAggregateFunction() {

		//Aggregate functions
		Query query1 = em.createQuery("select MAX(e.salary) from Employee e");
		
		Double result=(Double) query1.getSingleResult();
		System.out.println("Aggregate Function Results \n");
		System.out.println("Max Employee Salary : " + result);
	}
	
	/**
	 * Between, And and Like are the main keyword of JPQL
	 * these keywords are used after the Where clause in a query
	 */
	public static void showBetweenFunction() {
		System.out.println("Between Function JPQL \n");
		Query query = em.createQuery("select e " + "from Employee e " + "where e.salary " + "Between 3000 and 40000");
		
		List<Employee> empList = (List<Employee>)query.getResultList();
		
		for(Employee e:empList) {
			System.out.println("Employee id: " +e.getEid());
			System.out.println("\t Employee salary: " +e.getSalary());
		}
	}
	public static void showAndFunction() {
		System.out.println("And Function JPQL \n");
		Query query1 = em.
				createQuery("Select e " +
				"from Employee e " +
				"where e.ename LIKE 'M%'");
				List<Employee> list1=(List<Employee>)query1.getResultList( );
				for( Employee e:list1 )
				{
				System.out.print("Employee ID :"+e.getEid( ));
				System.out.println("\t Employee name :"+e.getName( ));
				}

	}
	public static void showLikeFunction() {
		System.out.println("Like Function JPQL \n");

	}
	/**
	 * to order the record in JPQL, we use ORDER BY clause
	 */
	public static void showOrdering() {
		
		System.out.println("GET UN-ARRANGE RESULT FIRST:");
		
		Query query = em.createQuery("select e from Employee e");
		List<Employee> list = (List<Employee>)query.getResultList();
		for(Employee e:list) {
			System.out.println("Employee ID: "+e.getEid());
			System.out.println("\t Employee Name: " +e.getName());
		}
		
		System.out.println("\n Arrange by ASC order");
		Query q = em.createQuery(" select e " + "from Employee e " + "ORDER BY e.ename ASC");
		
		List<Employee> elist = (List<Employee>)q.getResultList();
		
		for(Employee e:elist) {
			System.out.println("Employee ID: "+e.getEid());
			System.out.println("\t Employee Name: " +e.getName());
		}
	}
}
