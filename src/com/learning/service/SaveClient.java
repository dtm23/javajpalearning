package com.learning.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.learning.entity.NonTeachingStaff;
import com.learning.entity.TeachingStaff;

public class SaveClient {
	
	public static void main(String args[]) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Eclipselink_JPA");
		
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		//Teaching Staff entity
		
		TeachingStaff ts1= new TeachingStaff(1,"Teacher Devendra","MSc Computer Science","Java");
		
		TeachingStaff ts2= new TeachingStaff(2,"Teacher Thapa","MSc engineering","PHP");

		TeachingStaff ts3= new TeachingStaff(3,"Teacher Magar","BTEC ND IT Practitioner","Laravel");

		TeachingStaff ts4= new TeachingStaff(4,"Teacher Dvndra","MSc Computer Science","Python");

		//let's store all entities
		em.persist(ts1);
		em.persist(ts2);
		em.persist(ts3);
		em.persist(ts4);
		
		//Non-Teaching Staff
		NonTeachingStaff nts1= new NonTeachingStaff(5,"NonTeacher Mark","Font Desk");
		
		NonTeachingStaff nts2= new NonTeachingStaff(6,"NonTeacher Clark","Human Resource");
		//let store non teacing staff
		em.persist(nts1);
		em.persist(nts2);
		
		em.getTransaction().commit();
		em.close();
		emf.close();
		
	}

}
