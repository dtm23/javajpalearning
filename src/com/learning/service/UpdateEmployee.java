package com.learning.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.learning.Employee;

public class UpdateEmployee {
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Eclipselink_JPA");
		
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		Employee employee = em.find(Employee.class,1111);
		
		//show employee details before:
		System.out.println(employee.toString());
		
		// now let set a new value in salary
		employee.setSalary(203002030.00);
		em.getTransaction().commit();
		// show after update details of employee
		System.out.println("After update: " + employee.toString());
		em.close();
		emf.close();
		
	}

}
