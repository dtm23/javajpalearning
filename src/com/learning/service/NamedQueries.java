package com.learning.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.learning.Employee;

public class NamedQueries {
	
	public static void main(String[] args) {
		EntityManagerFactory emfactory = Persistence.
				createEntityManagerFactory( "Eclipselink_JPA" );
		EntityManager entitymanager = emfactory.createEntityManager();
		
		Query query = entitymanager.createNamedQuery("find Employee by id ");
				
		query.setParameter("eid", 1116);
			
		List<Employee> list = query.getResultList();
		
		for( Employee e:list ){
			System.out.print("Employee ID :"+e.getEid( ));
			System.out.println("\t Employee Name :"+e.getName( ));
		}
	}

}
