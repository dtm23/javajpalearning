package com.learning.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.learning.Employee;
public class FindEmployee {

	public static void main(String[] args) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Eclipselink_JPA");
		
		EntityManager em = emf.createEntityManager();
		
		Employee emp = em.find(Employee.class, 1111);
		
		//print emp details 
		System.out.println("Employee ID: "+emp.getEid());
		System.out.println("Employee Name: "+emp.getName());
		System.out.println("Employee Job titile: "+emp.getDeg());
		System.out.println("Employee Salary: "+emp.getSalary());
		
	}
}
