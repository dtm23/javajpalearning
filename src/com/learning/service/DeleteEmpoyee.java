package com.learning.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.learning.Employee;

/**
 * D
 * @author evers
 *
 */
public class DeleteEmpoyee {
	
	public static void main(String args[]) {
		try {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("Eclipselink_JPA");
			
			EntityManager em = emf.createEntityManager();
			
			em.getTransaction().begin();
			
			Employee emp = em.find(Employee.class,1111);
			
			em.remove(emp);
			em.getTransaction().commit();
			em.close();
			emf.close();
			System.out.println("Successfly deleted Emplyee id- " +emp.getEid() +", Employee name: " +emp.getName()+".");
		}catch(Exception e) {
			System.out.println("Error! - " + e.getLocalizedMessage());
		}
		
	}

}
