package com.learning.joinStrategy;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * @author evers
 *
 */

@Entity
@Table
@Inheritance(strategy = InheritanceType.JOINED)
public class JSStaff implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	
	
	
	/**
	 * 
	 * @param id
	 * @param name
	 */
	public JSStaff(int id,String name) {
		this.id = id;
		this.name = name;
	}

	public JSStaff() {
		super();
	}
	
	/**
	 * 
	 * @return id
	 */
	public int getID() {
		return id;
	}
	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * 
	 * @param id
	 */
	public void setID(int id) {
		this.id = id;
	}
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}
