package com.learning.joinStrategy;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName="id")
public class JSTeachingStaff extends JSStaff {
	
	private String qualification;
	private String subjectExpertise;
	
	public JSTeachingStaff(int id,String name,String qualification,String subjectExpertise) {
		super(id,name);

		this.qualification = qualification;
		this.subjectExpertise = subjectExpertise;
	}
	
	public JSTeachingStaff() {
		super();
	}
	
	/**
	 * 
	 * @param staffQualification
	 */
	public void setStaffQualifcation(String staffQualification) {
		this.qualification = staffQualification;
	}
	/**
	 * 
	 * @return qualification
	 */
	public String getStaffQualification() {
		return qualification;
	}
	/**
	 * 
	 * @param expertise
	 */
	public void setStaffSubjectExpertise(String expertise) {
		this.subjectExpertise = expertise;
	}
	/**
	 * 
	 * @return subject Expertise
	 */
	public String getStaffSubjectExpertise() {
		return subjectExpertise;
	}
}
