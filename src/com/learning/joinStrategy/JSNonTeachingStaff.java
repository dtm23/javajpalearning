package com.learning.joinStrategy;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@DiscriminatorValue(value="NS")
@PrimaryKeyJoinColumn(referencedColumnName="id")
public class JSNonTeachingStaff extends JSStaff{

	private String areaExpertise;
	/**
	 * 	
	 * @param area expertise (ae)
	 */
	public JSNonTeachingStaff(int id, String name,String ae) {
		super(id,name);
		this.areaExpertise = ae;
	}
	
	public JSNonTeachingStaff() {
		super();
	}
	/**
	 * 
	 * @param area expertise (ae)
	 */
	public void setAreaExpertise(String ae) {
		this.areaExpertise = ae;
	}
	/**
	 * 
	 * @return area expertise 
	 */
	public String getAreaExpertise() {
		return areaExpertise;
	}
}
