package com.learning.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue( value="TS")
public class TeachingStaff extends Staff {
	
	private String qualification;
	private String subjectExpertise;
	
	public TeachingStaff(int id,String name,String qualification,String subjectExpertise) {
		super(id,name);

		this.qualification = qualification;
		this.subjectExpertise = subjectExpertise;
	}
	
	public TeachingStaff() {
		super();
	}
	
	/**
	 * 
	 * @param staffQualification
	 */
	public void setStaffQualifcation(String staffQualification) {
		this.qualification = staffQualification;
	}
	/**
	 * 
	 * @return qualification
	 */
	public String getStaffQualification() {
		return qualification;
	}
	/**
	 * 
	 * @param expertise
	 */
	public void setStaffSubjectExpertise(String expertise) {
		this.subjectExpertise = expertise;
	}
	/**
	 * 
	 * @return subject Expertise
	 */
	public String getStaffSubjectExpertise() {
		return subjectExpertise;
	}
}
