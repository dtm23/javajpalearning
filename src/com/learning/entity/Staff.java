package com.learning.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * we look look at Inheritance Strategies
 * SINGLE_TABLE
 * JOINED_TABLE
 * TABLE_PER_CONCRETE_CLASS
 *  
 * @author evers
 *
 */
@Entity
@Table
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn( name="TYPE")
public class Staff implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	
	
	
	/**
	 * 
	 * @param id
	 * @param name
	 */
	public Staff(int id,String name) {
		this.id = id;
		this.name = name;
	}

	public Staff() {
		super();
	}
	
	/**
	 * 
	 * @return id
	 */
	public int getID() {
		return id;
	}
	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * 
	 * @param id
	 */
	public void setID(int id) {
		this.id = id;
	}
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}
