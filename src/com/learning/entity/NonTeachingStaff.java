package com.learning.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="NS")
public class NonTeachingStaff extends Staff{

	private String areaExpertise;
	/**
	 * 	
	 * @param area expertise (ae)
	 */
	public NonTeachingStaff(int id, String name,String ae) {
		super(id,name);
		this.areaExpertise = ae;
	}
	
	public NonTeachingStaff() {
		super();
	}
	/**
	 * 
	 * @param area expertise (ae)
	 */
	public void setAreaExpertise(String ae) {
		this.areaExpertise = ae;
	}
	/**
	 * 
	 * @return area expertise 
	 */
	public String getAreaExpertise() {
		return areaExpertise;
	}
}
