package com.learning;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Employee Entity POJO class
 * contain four entity: eid, ename, esalary, deg
 * @author evers
 * 
 *
 */
@Entity
@Table
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	private int eid;
	private String ename;
	private double salary;
	private String deg;
	
	/**
	 * 
	 * @param eid
	 * @param ename
	 * @param salary
	 * @param deg
	 */
	public Employee(int eid,String ename,double salary,String deg){
		super();
		this.eid = eid;
		this.ename = ename;
		this.salary = salary;
		this.deg = deg;
	}
	

	/**
	 * 
	 */
	public Employee(){
		super();
	}
	
	/**
	 * 
	 * @return eid
	 */
	public int getEid(){
		// return Employee id
		return eid;
	}
	/**
	 * 
	 * @param eid
	 */
	public void setEid(int eid){
		this.eid = eid;
	}
	/**
	 * 
	 * @return
	 */
	public String getName(){
		return ename;
	}
	/**
	 * 
	 * @param ename
	 */
	public void setEname(String ename){
		this.ename = ename;
	}
	/**
	 * 
	 * @return salary
	 */
	public double getSalary(){
		return salary;
	}
	/**
	 * 
	 * @param salary
	 */
	public void setSalary(double salary){
		this.salary = salary;
	}
	
	/**
	 * 
	 * @return deg
	 */
	public String getDeg(){
		return deg;
	}
	/**
	 * 
	 * @param deg
	 */
	public void setDeg(String deg){
		this.deg = deg;
	}
	
	/**
	 * return all info
	 */
	public String toString() {
		return "Employee [eid=" +eid +", ename=" +ename +", salary="+ salary+", deg=" + deg+". \n";
	}
}
